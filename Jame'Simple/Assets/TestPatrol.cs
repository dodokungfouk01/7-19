﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestPatrol : MonoBehaviour
{
    [SerializeField] 
    public Text _happy;
    public Text _gel;
    public Image _happypink;
    public Image _gelblue;
    private float happinessMax = 100;
    private float happiness;
    private float gelMax = 8;
    private float gel;
    void Start()
    {
        happiness = 100;
        gel = 8;
        //randomSpot = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(happiness > 100) {happiness = happinessMax;}
        if(gel > 8) {gel = gelMax;}
        if(gel < 0) {gel = 0;}
        //Score and Happiness Display
        _happypink.fillAmount = happiness / 100;
        _gelblue.fillAmount = gel / 8;
        _happy.text = "Happiness : " + System.Math.Round(happiness,0).ToString() + " / 100";
        _gel.text = "Gel : " + gel.ToString() + " / 8";
        //NoMaskInShop Happydown
        if(GameInstance.Instance.IsnoMask == true){
            happiness -= Time.deltaTime;
        }

    }
    public void IsNoMaskIn(){
        GameInstance.Instance.IsnoMask = true;
    }
    public void IsNoMaskOut(){
        GameInstance.Instance.IsnoMask = false;
    }

    public void UseGel()
    {
        if(gel == 0)happiness -= 5;
        gel--;
    }


}
