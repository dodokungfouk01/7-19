﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkTest : MonoBehaviour
{
    private CharacterController _controller;
    private Quaternion lookLeft;
    private Quaternion lookRight;
    private Quaternion lookForward;
    private Quaternion lookBack;
    bool task;
    bool holdingDown;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        lookRight = transform.rotation;
        lookLeft = lookRight * Quaternion.Euler(0, 180, 0);
        lookForward = lookRight * Quaternion.Euler(0, 90, 0);
        lookBack = lookRight * Quaternion.Euler(0, -90, 0);
        task = false;
    }
    // Update is called once per frame
    void Update()
    {
        Animator Anim = GetComponentInChildren<Animator>();
        if (!Input.anyKey && holdingDown)
        {
            Debug.Log("A key was released");
            holdingDown = false;
            Anim.SetBool("Walk", false);
        }
        if (Input.anyKey)
        {
            Debug.Log("A key is being pressed");
            holdingDown = true;
        }
        if (Input.GetKey(KeyCode.W) && holdingDown == true)
        {
            transform.rotation = lookLeft;
            Anim.SetBool("Walk", true);
        }
        if (Input.GetKey(KeyCode.A) && holdingDown == true)
        {
            transform.rotation = lookForward;
            Anim.SetBool("Walk", true);
        }
        if (Input.GetKey(KeyCode.S) && holdingDown == true)
        {
            Anim.SetBool("Walk", true);
            transform.rotation = lookRight;
        }
        if (Input.GetKey(KeyCode.D) && holdingDown == true)
        {
            transform.rotation = lookBack;
            Anim.SetBool("Walk", true);
        }
        if (Input.GetKeyDown(KeyCode.E) && BillCheck.Bill == true && task == false)
        {
            Debug.Log("bill");
            Anim.SetTrigger("Pick");
        }
    }

}