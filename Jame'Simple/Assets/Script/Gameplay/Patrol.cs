﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Patrol : MonoBehaviour {
    [SerializeField]
    public Text _text;
    public Text _score;
    public Text _happy;
    private double TextwaitTime;
    private float happinessMax = 100;
    private float happiness;
    private float score;
    public float speed;
    private float waitTime;
    public float startWaitTime;

    public Transform[] moveSpots;
    private int randomSpot;
    public Animator anim;
    public Transform target0;
    public Transform target1;
    public Transform target2;
    public Transform target3;
    public Transform target4;
    int WayPointCount = 0;
    // Start is called before the first frame update
    void Start () {
        waitTime = startWaitTime;
        happiness = 100;
        score = 0;
        randomSpot = 0;

    }

    // Update is called once per frame
    void Update () {
        anim = GetComponent<Animator> ();

        if (happiness >= 100) {
            happiness = happinessMax;
        }
        _score.text = "Score : " + score.ToString ();
        _happy.text = "Happiness : " + happiness.ToString () + " / 100";
        TextwaitTime = System.Math.Round (waitTime, 2);
        _text.text = "WaitTime : " + TextwaitTime.ToString ();
        transform.position = Vector3.MoveTowards (transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);
        if (Vector2.Distance (transform.position, moveSpots[randomSpot].position) < 0.05f) {
            anim.SetBool ("Walk", false);
            if (waitTime <= 0) {
                anim.SetBool ("Walk", true);
                if (randomSpot != 2) 
                {
                    randomSpot++;
                    WayPointCount++;
                    Debug.Log ("POint"+WayPointCount);
                }
                waitTime = startWaitTime;
                score += 100;
                happiness -= 10;
            } 
            else 
            {
                waitTime -= Time.deltaTime;
            }
            if(WayPointCount == 4)
            {
                transform.LookAt(target4.transform);
            }
            else if (WayPointCount == 3)
            {
                transform.LookAt(target3.transform);
            }
            else if (WayPointCount == 2)
            {
                transform.LookAt(target2.transform);
            }
            else if (WayPointCount == 1)
            {
                transform.LookAt(target1.transform);
            }
            else if (WayPointCount == 0)
            {
                transform.LookAt(target0.transform);
            }
        }
    }
}