﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInstance : MonoBehaviour
{
    static public GameInstance Instance
    {
        get
        {
            if (_singletonInstance == null)
            {
                _singletonInstance = GameObject.FindObjectOfType<GameInstance>();
                GameObject container = new GameObject("GameInstance");
                _singletonInstance = container.AddComponent<GameInstance>();
            }
            return _singletonInstance;
        }
    }
    static protected GameInstance _singletonInstance = null;

    public bool IsnoMask = false;
    public bool IsPlayerGotMask = false;
    public bool IsOptionMenuActive
    {
        get { return _isOptionMenuActive; }
        set { _isOptionMenuActive = value; }
    }

    protected bool _isOptionMenuActive = false;

    /*public int DifficultyLevel
    {
        get { return _difficultyLevel; }
        set { _difficultyLevel = value; }
    }
    protected int _difficultyLevel;
    public bool _isMusicEnabled
    {
        get { return _isMusicEnabled; }
        set { _isMusicEnabled = value; }
    }
    public bool SFXEnabled
    {
        get { return _isSFXEnabled; }
        set { _isSFXEnabled = value; }
    }
    protected bool _isSFXEnabled = true;*/
    public void GotMask(){
        IsPlayerGotMask = true;
    }
    public void DropMask(){
        IsPlayerGotMask = false;
    }
    void Awake()
    {
        if (_singletonInstance == null)
        {
            _singletonInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (this != _singletonInstance)
            {
                Destroy(this.gameObject);
            }
        }
    }
}