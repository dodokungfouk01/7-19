﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotNoMaskControl : MonoBehaviour
{
    [SerializeField] GameObject _bear;
    [SerializeField] GameObject _mhee;
    void Start()
    {
        GameInstance.Instance.IsPlayerGotMask = false;
    }

    public void ChangeTagBear(){
        if(GameInstance.Instance.IsPlayerGotMask == true){
            _bear.tag = "Bot";
        }
    }
    public void BotMaskOn(){
        if(GameInstance.Instance.IsPlayerGotMask == true){
            _mhee.SetActive(true);
        }
    }
    public void PutMaskOn(){
        if(GameInstance.Instance.IsPlayerGotMask == true){
            GameInstance.Instance.IsnoMask = false;
        } 
    }
    
}
