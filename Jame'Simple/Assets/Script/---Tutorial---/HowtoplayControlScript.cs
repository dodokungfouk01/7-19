﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HowtoplayControlScript : MonoBehaviour
{
    [SerializeField] Button _backButton;
    [SerializeField] Text _nametext;
    // Start is called before the first frame update
    void Start()
    {
        _nametext.text = "Name : " + GameApplicationManager.Instance.name;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BackButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneHowtoplay");
        SceneManager.LoadScene("SceneMainMenu");
    }
}
