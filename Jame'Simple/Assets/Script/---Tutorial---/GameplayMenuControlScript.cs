﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameplayMenuControlScript : MonoBehaviour
{
    [SerializeField] Button _backButton;
    [SerializeField] Button _nextButton;
    [SerializeField] Text _nametext;
    // Start is called before the first frame update
    void Start()
    {
        _nametext.text = "Name : " + GameApplicationManager.Instance.name;
        /*_backButton.onClick.AddListener
        (delegate { BackToMainMenuButtonClick(_backButton); });
        _nextButton.onClick.AddListener
        (delegate { BackToMainMenuButtonClick(_nextButton); });*/
    }

    // Update is called once per frame
    void Update()
    { 

    }
    public void BackToMainMenuButtonClick(Button button){
            SceneManager.UnloadSceneAsync("SceneGameplay");
            SceneManager.LoadScene("SceneMainMenu");
    } 
    public void NextStageButtonClick(Button button){
            //SceneManager.UnloadSceneAsync("SceneGameplay");
            SceneManager.LoadScene("SceneGameplay2");
    }

}
