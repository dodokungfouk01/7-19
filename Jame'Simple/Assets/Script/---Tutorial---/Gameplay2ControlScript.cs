﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gameplay2ControlScript : MonoBehaviour
{
    [SerializeField] Button _backButton;
    [SerializeField] Button _backStageButton;
    [SerializeField] Text _nametext;
    // Start is called before the first frame update
    void Start()
    {
        _nametext.text = "Name : " + GameApplicationManager.Instance.name;
        /*_backButton.onClick.AddListener
        (delegate { BackToMainMenuButtonClick(_backButton); });
        _backStageButton.onClick.AddListener
        (delegate { BackToMainMenuButtonClick(_backStageButton); });*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BackToMainMenuButtonClick(Button button){
            SceneManager.UnloadSceneAsync("SceneGameplay2");
            SceneManager.LoadScene("SceneMainMenu");
    } 
    public void BackStageButtonClick(Button button){
            SceneManager.UnloadSceneAsync("SceneGameplay2");
            SceneManager.LoadScene("SceneGameplay");
    }
}
