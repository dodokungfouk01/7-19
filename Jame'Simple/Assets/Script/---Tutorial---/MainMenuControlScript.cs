﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuControlScript : MonoBehaviour
{
    [SerializeField] Button _nameButton;
    [SerializeField] InputField _inputname;
    [SerializeField] Text _nametext;
    [SerializeField] Button _startButton;
    [SerializeField] Button _optionsButton;
    [SerializeField] Button _HowtoplayButton;
    [SerializeField] Button _Spawn1Button;
    [SerializeField] Button _Spawn2Button;
    [SerializeField] Button _exitButton;
    // Start is called before the first frame update
    void Start()
    {
        _nametext.text = "Name : " + GameApplicationManager.Instance.name;
        /*_startButton.onClick.AddListener(delegate { StartButtonClick(_startButton); });
        _optionsButton.onClick.AddListener(delegate { OptionsButtonClick(_optionsButton); });
        _exitButton.onClick.AddListener(delegate { ExitButtonClick(_exitButton); });*/
    }
    void Update()
    {
        
    }
    public void NameButtonClick(Button button)
    {
        GameApplicationManager.Instance.name = _inputname.text.ToString();
        _nametext.text = "Name : " + GameApplicationManager.Instance.name;
        //Debug.Log("1 2 3 4 " + GameApplicationManager.Instance.name);
    }

    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("SceneGameplay");
    }
    public void OptionsButtonClick(Button button)
    {
        if (!GameApplicationManager.Instance.IsOptionMenuActive)
        {
            SceneManager.LoadScene("SceneOptions", LoadSceneMode.Additive);
            GameApplicationManager.Instance.IsOptionMenuActive = true;
        }
    }
    public void HowtoplayClick(Button button)
    {
        SceneManager.LoadScene("SceneHowtoplay");
    }
    public void Spawn1Click(Button button)
    {
        SceneManager.LoadScene("SpawnItemObst1");
    }
    public void Spawn2Click(Button button)
    {
        SceneManager.LoadScene("SpawnItemObstAddFBX3");
    }
    public void ExitButtonClick(Button button)
    {
        /*Application.Quit();
        SceneManager.UnloadSceneAsync("SceneMainMenu");
        SceneManager.LoadScene("SceneExit");*/
    }
}
